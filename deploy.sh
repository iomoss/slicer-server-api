#!/bin/bash

## Create output folder
echo -e "Creating output directory...\c"
mkdir -p html
echo "OK"

## Handle seqdiag
echo -e "Creating sequence diagrams...\c"
find diagrams -name "*.seq" -exec seqdiag -Tsvg -o "{}.svg" "{}" \;
echo "OK"

echo -e "Creating block diagrams...\c"
find diagrams -name "*.blk" -exec blockdiag -Tsvg -o "{}.svg" "{}" \;
echo "OK"

echo -e "Creating network diagrams...\c"
find diagrams -name "*.nwd" -exec nwdiag -Tsvg -o "{}.svg" "{}" \;
echo "OK"

echo -e "Moving diagrams to output...\c"
mv diagrams/*.svg html/
echo "OK"

## Run the docker
echo "Compiling API to HTML..."
docker run -it -v $PWD:/srv/ davidonlaptop/aglio:2.1.0 -i /srv/src/main.apib -o /srv/html/output.html
echo "OK"

## Clean up
echo -e "Cleaning up...\c"
echo "OK"

# docker run --rm -it -v $PWD:/srv/ -p 8000:3000 davidonlaptop/aglio:2.1.0 -i /srv/src/main.apib --server --host="0.0.0.0"
